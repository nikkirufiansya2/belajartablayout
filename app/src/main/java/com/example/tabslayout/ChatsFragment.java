package com.example.tabslayout;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


public class ChatsFragment extends Fragment {
    int images[]  = {R.drawable.dinda,
                    R.drawable.adinda,
                    R.drawable.dinda,
                    R.drawable.adinda,
                    R.drawable.dinda,
                    R.drawable.adinda,};
    String[] name = {"dina","cindy","ayu", "anggi" ,"intan", "wulan"};
    String[] message = {"Haii","Pagi","Apa Kabar","Lagi Ngapain","Keluar Yuks","Lagi sibuk kah"};
    ListView  listView;
    ListAdapter lAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats, container, false);
        listView = view.findViewById(R.id.listview);
        lAdapter = new ListAdapter(getContext(), name, message, images);
        listView.setAdapter(lAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getContext(), name[i], Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}